﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _6_ASP_DataBase.SQL
{
    public class DMBS
    {
        static public readonly bool DEBUG_GERAL = false;

        public const int DBMS_NULL = 0;
        public const int DBMS_SQLSERVER_BD_LOCAL = 1;
        public const int DBMS_SQLSERVER = 2;
        public const int DBMS_MYSQL = 3;
        public const int DBMS_ORACLE = 4;
        public const int DBMS_SQLITE = 5;
    }
}