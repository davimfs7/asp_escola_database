﻿using System;
using System.Text;
using System.Collections.Generic;

//---------------SQL-------------------------
using static _6_ASP_DataBase.SQL.SQL_Connection;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Data.SQLite;
using Oracle.DataAccess.Client;
using static _6_ASP_DataBase.SQL.DMBS;

namespace _6_ASP_DataBase.SQL
{
    /// <summary>
    /// CRUD da Entidade Aluno
    /// </summary>
    static public class SQLaluno
    {
        #region Dados Locais

        private const bool DEBUG_LOCAL = false;       // Ativa debug local

        #endregion

        #region Create

        /// <summary>
        /// Adiciona um novo registo à tabela
        /// </summary>
        /// <param name="user"></param>
        static public void Add(Aluno aluno)
        {
            // Imprime DEBUG para a consola, se DEBUG local e GERAL estiverem ativos
            if (DEBUG_GERAL && DEBUG_LOCAL)
            {
                Console.WriteLine("Debug: SQLaluno - add() - <----Iniciar Query---->");
                Console.WriteLine("Debug: SQLaluno - add() - DBMS ATIVO: " + DBMSactive);
            }

            //Execução do SQL DML sob controlo do try catch
            try
            {
                // Abre ligação ao DBMS Ativo
                using (DbConnection conn = OpenConnection())
                {
                    // Execução do SQL DML em função do DBMS Ativo
                    switch (DBMSactive)
                    {
                        case DBMS_SQLSERVER:
                        case DBMS_SQLSERVER_BD_LOCAL:
                            // Prepara e executa o SQL DML
                            using (SqlCommand sqlCommand = ((SqlConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "INSERT INTO \"Aluno\""
                                + "(Nome, CodCivil, Morada, DataNascimento, Niss, Nif, TelefoneFixo, Telemovel, Email, Genero, Nacionalidade, Naturalidade)"
                                + "VALUES(@nome,@codcivil,@morada,@datanascimento,@niss,@nif,@telfixo,@telmovel,@email,@genero,@nac,@nat);";
                                sqlCommand.Parameters.Add(new SqlParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new SqlParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new SqlParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new SqlParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new SqlParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new SqlParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new SqlParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new SqlParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new SqlParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new SqlParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new SqlParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new SqlParameter("@nat", aluno.Naturalidade));

                                // Tenta Executar e Se diferente de 1, provoca excessão saltanto para o catch
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    throw new InvalidProgramException("SQLaluno - add() - sqlserver: ");
                                }
                            }
                            break;

                        case DBMS_MYSQL:
                            // Prepara e executa o SQL DML
                            using (MySqlCommand sqlCommand = ((MySqlConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "INSERT INTO `escola`.`aluno`"
                                + "(`Nome`, `CodCivil`, `Morada`, `DataNascimento`, `Niss`, `Nif`, `TelefoneFixo`, `Telemovel`, `Email`, `Genero`, `Nacionalidade`, `Naturalidade`)"
                                + "VALUES(@nome,@codcivil,@morada,@datanascimento,@niss,@nif,@telfixo,@telmovel,@email,@genero,@nac,@nat);";
                                sqlCommand.Parameters.Add(new MySqlParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new MySqlParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new MySqlParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new MySqlParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new MySqlParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new MySqlParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new MySqlParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new MySqlParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new MySqlParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new MySqlParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new MySqlParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new MySqlParameter("@nat", aluno.Naturalidade));

                                // Tenta Executar e Se diferente de 1, provoca excessão saltanto para o catch
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    throw new InvalidProgramException("SQLaluno - add() - mysql: ");
                                }
                            }
                            break;

                        case DBMS_ORACLE:
                            // Prepara e executa o SQL DML
                            using (OracleCommand sqlCommand = ((OracleConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "INSERT INTO \"Aluno\""
                                + "(Nome, CodCivil, Morada, DataNascimento, Niss, Nif, TelefoneFixo, Telemovel, Email, Genero, Nacionalidade, Naturalidade)"
                                + "VALUES(@nome,@codcivil,@morada,@datanascimento,@niss,@nif,@telfixo,@telmovel,@email,@genero,@nac,@nat);";
                                sqlCommand.Parameters.Add(new OracleParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new OracleParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new OracleParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new OracleParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new OracleParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new OracleParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new OracleParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new OracleParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new OracleParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new OracleParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new OracleParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new OracleParameter("@nat", aluno.Naturalidade));

                                // Tenta Executar e Se diferente de 1, provoca excessão saltanto para o catch
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    throw new InvalidProgramException("SQLaluno - add() - Oracle: ");
                                }
                            }
                            break;

                        case DBMS_SQLITE:
                            // Prepara e executa o SQL DML
                            using (SQLiteCommand sqlCommand = ((SQLiteConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "INSERT INTO \"Aluno\""
                                + "(Nome, CodCivil, Morada, DataNascimento, Niss, Nif, TelefoneFixo, Telemovel, Email, Genero, Nacionalidade, Naturalidade)"
                                + "VALUES(@nome,@codcivil,@morada,@datanascimento,@niss,@nif,@telfixo,@telmovel,@email,@genero,@nac,@nat);";
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nat", aluno.Naturalidade));

                                // Tenta Executar e Se diferente de 1, provoca excessão saltanto para o catch
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    throw new InvalidProgramException("SQLaluno - add() - SQlite: ");
                                }
                            }
                            break;

                        default:
                            Console.WriteLine(
                                "O DBMS Ativo não está definido: SQLturma - add()",
                                "SQLaluno - Add() - Switch(DBMSactive)"
                            );
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erro: SQLuser_mors - add() - \n" + e.ToString());
                Console.WriteLine(
                    "SQLaluno - Add() - \n Ocorreram problemas com a ligação à Base de Dados: \n" + e.ToString(),
                    "SQLaluno - Add() - Catch"
                );
                throw e;
            }
        }
        #endregion

        #region Read

        /// <summary>
        /// Recolhe todos os registos da tabela, converte para obj e adiciona numa lista a devolver
        /// Há várias ligações - Processo:
        /// 1 - Ligação BD - Extrai os registos BD para a lista principal
        /// 2 - Completa os objeto da lista principal, preenchendo os obj FK, com base nas listas FK
        ///     Usa um ciclo para correr a lista principal e um ciclo ou obj FK
        /// </summary>
        /// <returns>Lista de objetos</returns>
        static public List<Aluno> GetAll()
        {
            List<Aluno> listaAlunos = new List<Aluno>();   // Lista Principal
            String query = "";

            if (DEBUG_GERAL && DEBUG_LOCAL)
            {
                Console.WriteLine("Debug: SQLaluno - getAll() - <----Iniciar Query---->");
                Console.WriteLine("Debug: SQLaluno - getAll() - DBMS ATIVO: " + DBMSactive);
            }

            //Execução do SQL DML sob controlo do try catch
            try
            {
                // Abre ligação ao DBMS Ativo
                using (DbConnection conn = OpenConnection())
                {
                    // Execução do SQL DML em função do DBMS Ativo
                    switch (DBMSactive)
                    {
                        case DBMS_SQLSERVER:
                        case DBMS_SQLSERVER_BD_LOCAL:
                            query = "SELECT * FROM Aluno;";

                            // Prepara e executa o SQL DML
                            using (SqlCommand sqlCommand = new SqlCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandText = query;
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((SqlConnection)conn);

                                // DEBUG
                                if (DEBUG_GERAL && DEBUG_LOCAL)
                                {
                                    Console.WriteLine("Debug: SQLaluno - getAll() - SQLSERVER - SQLcommand OK");
                                }

                                // Reader recebe os dados da execução da query
                                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - getAll() - SQLSERVER - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    while (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        Aluno aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }

                                        //adiciona o obj à lista
                                        listaAlunos.Add(aluno);       
                                    }
                                }
                            }
                            break;
                        case DBMS_MYSQL:
                            query = "SELECT * FROM `escola`.`aluno`;";

                            // Prepara e executa o SQL DML
                            using (MySqlCommand sqlCommand = new MySqlCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandText = query;
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((MySqlConnection)conn);

                                // DEBUG
                                if (DEBUG_GERAL && DEBUG_LOCAL)
                                {
                                    Console.WriteLine("Debug: SQLaluno - getAll() - MYSQL - SQLcommand OK");
                                }

                                // Reader recebe os dados da execução da query
                                using (MySqlDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - getAll() - MYSQL - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    while (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        Aluno aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }

                                        //adiciona o obj à lista
                                        listaAlunos.Add(aluno);
                                    }
                                }
                            }
                            break;

                        case DBMS_ORACLE:
                            query = "SELECT * FROM Aluno;";

                            // Prepara e executa o SQL DML
                            using (OracleCommand sqlCommand = new OracleCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandText = query;
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((OracleConnection)conn);

                                // DEBUG
                                if (DEBUG_GERAL && DEBUG_LOCAL)
                                {
                                    Console.WriteLine("Debug: SQLaluno - getAll() - ORACLE - SQLcommand OK");
                                }

                                // Reader recebe os dados da execução da query
                                using (OracleDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - getAll() - ORACLE - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    while (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        Aluno aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }

                                        //adiciona o obj à lista
                                        listaAlunos.Add(aluno);
                                    }
                                }
                            }
                            break;

                        case DBMS_SQLITE:
                            query = "SELECT * FROM Aluno;";

                            // Prepara e executa o SQL DML
                            using (SQLiteCommand sqlCommand = new SQLiteCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandText = query;
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((SQLiteConnection)conn);

                                // DEBUG
                                if (DEBUG_GERAL && DEBUG_LOCAL)
                                {
                                    Console.WriteLine("Debug: SQLaluno - getAll() - SQLITE - SQLcommand OK");
                                }

                                // Reader recebe os dados da execução da query
                                using (SQLiteDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - getAll() - SQLITE - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    while (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        Aluno aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }

                                        //adiciona o obj à lista
                                        listaAlunos.Add(aluno);
                                    }
                                }
                            }
                            break;
                        default:
                            Console.WriteLine(
                                "O DBMS Ativo não está definido: SQLturma - add()" + 
                                "SQLaluno - GetAll() - Switch(DBMSactive)"
                            );
                            break;
                    }
                }

                // Se Entidade tem FKs, Executar um foreach à lista de objetos extraídos e completar com o objeto fk, usando um get(id) à SQL respetiva

            }
            catch (Exception e)
            {
                Console.WriteLine("Erro: SQLaluno - getAll() - \n" + e.ToString());
                Console.WriteLine(
                    "SQLaluno - GetAll() - \n Ocorreram problemas com a ligação à Base de Dados: \n" + e.ToString(),
                    "SQLaluno - GetAll() - Catch"
                );
                return null;
            }
            return listaAlunos;
        }

        /// <summary>
        /// Obtem um registo completo da tabela através do seu id, converte para obj e devolve.
        /// Há várias ligações - Processo:
        /// 1 - Ligação BD - Extrai o registo BD.
        /// 2 - Completa os objeto da lista principal, preenchendo os obj FK, com base nas listas FK
        ///     Usa um ciclo para correr a lista principal e um ciclo ou obj FK
        /// </summary>
        /// <returns>Devolve um objeto preenchido ou NULL</returns>
        public static Aluno Get(int id)
        {
            Aluno aluno = null;

            if (DEBUG_GERAL && DEBUG_LOCAL)
            {
                Console.WriteLine("Debug: SQLaluno - get() - <----Iniciar Query---->");
                Console.WriteLine("Debug: SQLaluno - get() - DBMS ATIVO: " + DBMSactive);
            }

            //Execução do SQL DML sob controlo do try catch
            try
            {
                // Abre ligação ao DBMS Ativo
                using (DbConnection conn = OpenConnection())
                {
                    // Execução do SQL DML em função do DBMS Ativo
                    switch (DBMSactive)
                    {
                        case DBMS_SQLSERVER:
                        case DBMS_SQLSERVER_BD_LOCAL:
                            // Prepara e executa o SQL DML
                            using (SqlCommand sqlCommand = new SqlCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((SqlConnection)conn);

                                // SQL DDL
                                sqlCommand.CommandText = "SELECT * FROM Aluno where Id=@Id;";
                                sqlCommand.Parameters.Add(new SqlParameter("@id", id));

                                // Reader recebe os dados da execução da query
                                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - get() - SQLSERVER - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    if (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }
                                    }
                                }
                            }
                            break;

                        case DBMS_MYSQL:
                            // Prepara e executa o SQL DML
                            using (MySqlCommand sqlCommand = new MySqlCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((MySqlConnection)conn);

                                // SQL DDL
                                sqlCommand.CommandText = "SELECT * FROM Aluno where Id=@Id;";
                                sqlCommand.Parameters.Add(new MySqlParameter("@id", id));

                                // Reader recebe os dados da execução da query
                                using (MySqlDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - get() - MYSQL - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    if (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }
                                    }
                                }
                            }
                            break;
                        case DBMS_ORACLE:
                            // Prepara e executa o SQL DML
                            using (OracleCommand sqlCommand = new OracleCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((OracleConnection)conn);

                                // SQL DDL
                                sqlCommand.CommandText = "SELECT * FROM Aluno where Id=@Id;";
                                sqlCommand.Parameters.Add(new SqlParameter("@id", id));

                                // Reader recebe os dados da execução da query
                                using (OracleDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - get() - ORACLE - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    if (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }
                                    }
                                }
                            }
                            break;
                        case DBMS_SQLITE:
                            // Prepara e executa o SQL DML
                            using (SQLiteCommand sqlCommand = new SQLiteCommand())
                            {
                                // Config da ligação
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.Connection = ((SQLiteConnection)conn);

                                // SQL DDL
                                sqlCommand.CommandText = "SELECT * FROM Aluno where Id=@Id;";
                                sqlCommand.Parameters.Add(new SqlParameter("@id", id));

                                // Reader recebe os dados da execução da query
                                using (SQLiteDataReader reader = sqlCommand.ExecuteReader())
                                {
                                    if (DEBUG_GERAL && DEBUG_LOCAL)
                                    {
                                        Console.WriteLine("Debug: SQLaluno - get() - SQLITE - DATAREADER CRIADO");
                                    }

                                    // Extração dos dados do reader para a lista, um a um: registo tabela -> new Obj ->Lista<Objs>
                                    if (reader.Read())
                                    {
                                        //Debug para Output: Interessa ver o que está a sair do datareader
                                        if (DEBUG_GERAL && DEBUG_LOCAL)
                                        {
                                            Console.WriteLine("Debug: SQLaluno - getAll(): SQLSERVER - DATAREADER TEM REGISTOS!");
                                            Console.WriteLine("Id->" + reader.GetInt32(reader.GetOrdinal("ID")).ToString());
                                            Console.WriteLine("Nome -> " + reader["Nome"].ToString());
                                            Console.WriteLine("CodCivil -> " + reader.GetInt32(reader.GetOrdinal("CodCivil")).ToString());
                                            Console.WriteLine("Morada-> " + reader["Morada"].ToString());
                                            Console.WriteLine("DataNascimento -> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("DataNascimento-> " + reader.GetDateTime(reader.GetOrdinal("DataNascimento")).ToString());
                                            Console.WriteLine("Niss -> " + reader.GetInt32(reader.GetOrdinal("Niss")).ToString());
                                            Console.WriteLine("Nif -> " + reader.GetInt32(reader.GetOrdinal("Nif")).ToString());
                                            Console.WriteLine("TelefoneFixo -> " + reader.GetInt32(reader.GetOrdinal("TelefoneFixo")).ToString());
                                            Console.WriteLine("TelefoneMovel -> " + reader.GetInt32(reader.GetOrdinal("TelefoneMovel")).ToString());
                                            Console.WriteLine("Email -> " + reader["Email"].ToString());
                                            Console.WriteLine("Genero-> " + reader.GetInt32(reader.GetOrdinal("Genero")).ToString());
                                            Console.WriteLine("Nacionalidade -> " + reader["Nacionalidade"].ToString());
                                            Console.WriteLine("Naturalidade -> " + reader["Naturalidade"].ToString());
                                        }

                                        // Construção do objeto (com construtor mínimo)
                                        // Se objeto tem FKs, Não usar SQL***.get() para construir o objeto fk dentro do construtor. gera exceção.
                                        // Criar o obj FK com o Construtor de Id e depois completar o objeto a seguir à connection.close.
                                        aluno = new Aluno(
                                            reader.GetInt32(reader.GetOrdinal("ID")),
                                            reader["Nome"].ToString(),
                                            reader.GetDateTime(reader.GetOrdinal("DataNascimento")),
                                            //reader.GetInt32(reader.GetOrdinal("Genero")) == 1
                                            reader.GetBoolean(reader.GetOrdinal("Genero"))
                                        );

                                        //----Preencher outros campos----//

                                        // Atributos NULLABLES, Só são preenchidos no objeto caso não estejam DBnull na tabela
                                        if (!reader.IsDBNull(reader.GetOrdinal("CodCivil")))
                                        {
                                            aluno.CodCivil = reader.GetInt32(reader.GetOrdinal("CodCivil"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Morada")))
                                        {
                                            aluno.Morada = reader["Morada"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Niss")))
                                        {
                                            aluno.Niss = reader.GetInt32(reader.GetOrdinal("Niss"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nif")))
                                        {
                                            aluno.Nif = reader.GetInt32(reader.GetOrdinal("Nif"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("TelefoneFixo")))
                                        {
                                            aluno.TelefoneFixo = reader.GetInt32(reader.GetOrdinal("TelefoneFixo"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Telemovel")))
                                        {
                                            aluno.Telemovel = reader.GetInt32(reader.GetOrdinal("Telemovel"));
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Email")))
                                        {
                                            aluno.Email = reader["Email"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Nacionalidade")))
                                        {
                                            aluno.Nacionalidade = reader["Nacionalidade"].ToString();
                                        }

                                        if (!reader.IsDBNull(reader.GetOrdinal("Naturalidade")))
                                        {
                                            aluno.Naturalidade = reader["Naturalidade"].ToString();
                                        }
                                    }
                                }
                            }
                            break;
                        default:
                            Console.WriteLine(
                                "O DBMS Ativo não está definido: SQLturma - add()",
                                "SQLaluno - Get() - Switch(DBMSactive)"
                            );
                            break;
                    }
                }

                // Se Entidade tem FKs, completar o objeto extraído com o objeto fk, com um get(id) à SQL respetiva

            }
            catch (Exception e)
            {
                Console.WriteLine("Erro: SQLaluno - get() - \n" + e.ToString());
                Console.WriteLine(
                    "SQLaluno - Get() - \n Ocorreram problemas com a ligação à Base de Dados: \n" + e.ToString(),
                    "SQLaluno - Get() - Catch"
                );
                return null;
            }
            return aluno;
        }

        #endregion

        #region Update

        /// <summary>
        /// Altera um registo da tabela
        /// </summary>
        /// <param name="aluno">Objeto com id a alterar da tabela</param>
        static public void Set(Aluno aluno)
        {
            if (DEBUG_GERAL && DEBUG_LOCAL)
            {
                Console.WriteLine("Debug: SQLaluno - set() - <----Iniciar Query---->");
                Console.WriteLine("Debug: SQLaluno - set() - DBMS ATIVO: " + DBMSactive);
            }

            //Execução do SQL DML sob controlo do try catch
            try
            {
                // Abre ligação ao DBMS Ativo
                using (DbConnection conn = OpenConnection())
                {
                    // Execução do SQL DML em função do DBMS Ativo
                    switch (DBMSactive)
                    {
                        case DBMS_SQLSERVER:
                        case DBMS_SQLSERVER_BD_LOCAL:
                            // Prepara e executa o SQL DML
                            using (SqlCommand sqlCommand = ((SqlConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "UPDATE Aluno SET "
                                + " Nome = @nome,"
                                + " CodCivil = @codcivil,"
                                + " Morada = @morada,"
                                + " DataNascimento = @datanascimento,"
                                + " Niss = @niss,"
                                + " Nif = @nif,"
                                + " TelefoneFixo = @telfixo,"
                                + " Telemovel = @telmovel,"
                                + " Email = @email,"
                                + " Genero = @genero,"
                                + " Nacionalidade = @nac,"
                                + " Naturalidade = @nat"
                                + " WHERE Id = @id;";
                                sqlCommand.Parameters.Add(new SqlParameter("@id", aluno.Id));
                                sqlCommand.Parameters.Add(new SqlParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new SqlParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new SqlParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new SqlParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new SqlParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new SqlParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new SqlParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new SqlParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new SqlParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new SqlParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new SqlParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new SqlParameter("@nat", aluno.Naturalidade));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - set() - sqlserver: ");
                                }
                            }
                            break;

                        case DBMS_MYSQL:
                            // Prepara e executa o SQL DML
                            using (MySqlCommand sqlCommand = ((MySqlConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "UPDATE `escola`.`aluno` SET "
                                + " `Nome` = @nome,"
                                + " `CodCivil` = @codcivil,"
                                + " `Morada` = @morada,"
                                + " `DataNascimento` = @datanascimento,"
                                + " `Niss` = @niss,"
                                + " `Nif` = @nif,"
                                + " `TelefoneFixo` = @telfixo,"
                                + " `Telemovel` = @telmovel,"
                                + " `Email` = @email,"
                                + " `Genero` = @genero,"
                                + " `Nacionalidade` = @nac,"
                                + " `Naturalidade` = @nat"
                                + " WHERE Id = @id;";
                                sqlCommand.Parameters.Add(new MySqlParameter("@id", aluno.Id));
                                sqlCommand.Parameters.Add(new MySqlParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new MySqlParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new MySqlParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new MySqlParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new MySqlParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new MySqlParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new MySqlParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new MySqlParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new MySqlParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new MySqlParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new MySqlParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new MySqlParameter("@nat", aluno.Naturalidade));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - set() - mysql: ");
                                }
                            }
                            break;
                        case DBMS_ORACLE:
                            // Prepara e executa o SQL DML
                            using (OracleCommand sqlCommand = ((OracleConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "UPDATE Aluno SET "
                                + " Nome = @nome,"
                                + " CodCivil = @codcivil,"
                                + " Morada = @morada,"
                                + " DataNascimento = @datanascimento,"
                                + " Niss = @niss,"
                                + " Nif = @nif,"
                                + " TelefoneFixo = @telfixo,"
                                + " Telemovel = @telmovel,"
                                + " Email = @email,"
                                + " Genero = @genero,"
                                + " Nacionalidade = @nac,"
                                + " Naturalidade = @nat"
                                + " WHERE Id = @id;";
                                sqlCommand.Parameters.Add(new OracleParameter("@id", aluno.Id));
                                sqlCommand.Parameters.Add(new OracleParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new OracleParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new OracleParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new OracleParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new OracleParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new OracleParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new OracleParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new OracleParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new OracleParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new OracleParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new OracleParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new OracleParameter("@nat", aluno.Naturalidade));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - set() - Oracle: ");
                                }
                            }
                            break;

                        case DBMS_SQLITE:
                            // Prepara e executa o SQL DML
                            using (SQLiteCommand sqlCommand = ((SQLiteConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "UPDATE Aluno SET "
                                + " Nome = @nome,"
                                + " CodCivil = @codcivil,"
                                + " Morada = @morada,"
                                + " DataNascimento = @datanascimento,"
                                + " Niss = @niss,"
                                + " Nif = @nif,"
                                + " TelefoneFixo = @telfixo,"
                                + " Telemovel = @telmovel,"
                                + " Email = @email,"
                                + " Genero = @genero,"
                                + " Nacionalidade = @nac,"
                                + " Naturalidade = @nat"
                                + " WHERE Id = @id;";
                                sqlCommand.Parameters.Add(new SQLiteParameter("@id", aluno.Id));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nome", aluno.Nome));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@codcivil", aluno.CodCivil));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@morada", aluno.Morada));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@datanascimento", aluno.DataNascimento));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@niss", aluno.Niss));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nif", aluno.Nif));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@telfixo", aluno.TelefoneFixo));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@telmovel", aluno.Telemovel));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@email", aluno.Email));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@genero", aluno.Genero));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nac", aluno.Nacionalidade));
                                sqlCommand.Parameters.Add(new SQLiteParameter("@nat", aluno.Naturalidade));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - set() - SQLite: ");
                                }
                            }
                            break;
                        default:
                            Console.WriteLine(
                                "O DBMS Ativo não está definido: SQLturma - add()",
                                "SQLaluno - Add() - Switch(DBMSactive)"
                            );
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erro: SQLaluno - set() - \n" + e.ToString());
                Console.WriteLine(
                    "SQLaluno - Set() - \n Ocorreram problemas com a ligação à Base de Dados: \n" + e.ToString(),
                    "SQLaluno - Set() - Catch"
                );
                throw e;
            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// Delete de um registo da tabela. 
        /// ATENÇÃO: Porque estes objetos são FK noutras tabelas, o delete aplica-se após 
        /// o método checkRelationalIntegrityViolation(), caso contrário pode gerar Exceções
        /// </summary>
        /// <param name="aluno">Objeto com id a apagar da tabela</param>
        static public void Del(Aluno aluno)
        {
            if (DEBUG_GERAL && DEBUG_LOCAL)
            {
                Console.WriteLine("Debug: SQLaluno - del() - <----Iniciar Query---->");
                Console.WriteLine("Debug: SQLaluno - del() - DBMS ATIVO: " + DBMSactive);
            }

            //Execução do SQL DML sob controlo do try catch
            try
            {
                // Abre ligação ao DBMS Ativo
                using (DbConnection conn = OpenConnection())
                {
                    // Execução do SQL DML em função do DBMS Ativo
                    switch (DBMSactive)
                    {
                        case DBMS_SQLSERVER:
                        case DBMS_SQLSERVER_BD_LOCAL:
                            // Prepara e executa o SQL DML
                            using (SqlCommand sqlCommand = ((SqlConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "DELETE FROM Aluno WHERE Id=@id;";
                                sqlCommand.Parameters.Add(new SqlParameter("@id", aluno.Id));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - del() - sqlserver: ");
                                }
                            }
                            break;

                        case DBMS_MYSQL:
                            using (MySqlCommand sqlCommand = ((MySqlConnection)conn).CreateCommand())
                            {
                                // Prepara e executa o SQL DML
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "DELETE FROM `escola`.`aluno` WHERE `Id`=@id;";
                                sqlCommand.Parameters.Add(new MySqlParameter("@id", aluno.Id));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - del() - mysql: ");
                                }
                            }
                            break;
                        case DBMS_ORACLE:
                            // Prepara e executa o SQL DML
                            using (OracleCommand sqlCommand = ((OracleConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "DELETE FROM Aluno WHERE Id=@id;";
                                sqlCommand.Parameters.Add(new OracleParameter("@id", aluno.Id));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - del() - DBMS_ORACLE: ");
                                }
                            }
                            break;

                        case DBMS_SQLITE:
                            // Prepara e executa o SQL DML
                            using (SQLiteCommand sqlCommand = ((SQLiteConnection)conn).CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = "DELETE FROM Aluno WHERE Id=@id;";
                                sqlCommand.Parameters.Add(new SQLiteParameter("@id", aluno.Id));

                                // Tenta executar o comando, que é suposto devolver 1
                                if (sqlCommand.ExecuteNonQuery() != 1)
                                {
                                    // Se diferente, inverte o commit e Provoca a excessão saltanto para o catch
                                    throw new InvalidProgramException("SQLaluno - del() - DBMS_SQLITE: ");
                                }
                            }
                            break;
                        default:
                            Console.WriteLine(
                                "O DBMS Ativo não está definido: SQLturma - add()",
                                "SQLaluno - Del() - Switch(DBMSactive)"
                            );
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erro: SQLaluno - del() - \n" + e.ToString());
                Console.WriteLine(
                    "SQLaluno - Del() - \n Ocorreram problemas com a ligação à Base de Dados: \n" + e.ToString(),
                    "SQLaluno - Del() - Catch"
                );
                throw e;
            }
        }

        /// <summary>
        /// Controlo de Violação de Integridade Relacional. 
        /// Aplica-se antes do del(). 
        /// A não utilização em PAR destes métodos, vai gerar Exceções
        /// </summary>
        /// <param name="aluno">Registo a testar</param>
        /// <returns></returns>
        //static public bool CheckRelationalIntegrityViolation(Aluno aluno)
        //{
        //    if (DEBUG_GERAL && DEBUG_LOCAL)
        //    {
        //        Console.WriteLine("Debug: SQLusers - checkRelationalIntegrityViolation() - <----Iniciar Query---->");
        //        Console.WriteLine("Debug: SQLusers - checkRelationalIntegrityViolation() - DBMS ATIVO: " + DBMSactive);
        //    }


        //    ////////////////////////////////////////////////////////////////////////////////////////////////
        //    // Controlo de Violação de Inegridade Relacional:
        //    // Verifica se o registo em delete, existe nas tabelas relacionadas (com FK para esta tabela)
        //    // Analisar no DER as tabelas a tratar: Turma
        //    ////////////////////////////////////////////////////////////////////////////////////////////////
        //    StringBuilder strBuilderFK = new StringBuilder();    // Recebe a info onde há violação de integridade
        //    strBuilderFK.AppendLine("Para eliminar este registo, precisa primeiro de eliminar os seus movimentos em:");

        //    // Flag de controlo de violação de interidade, para ativar as mensagens na FormAuxuliarInfo
        //    bool relationalViolationForFKtables = false;   // ativa-se quando o user é fk em tabelas relacionadas

        //    int count;  // Acumula o nº de ocorrências positivas:

        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    // Para cada tabela referenciada acima, puxa a lista e verifica se tem o registo a eliminar.
        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //    // verifica se há FKs em Matricula
        //    count = 0;
        //    foreach (Matricula matricula in SQLmatricula.GetAll())
        //    {
        //        if (matricula.Aluno.Id == aluno.Id)
        //        {
        //            count++;
        //            relationalViolationForFKtables = true;
        //        }
        //    }
        //    if (count > 0) strBuilderFK.AppendLine("- Matricula (" + count + "); ");

        //    // Outras tabelas aqui.

        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    // Comunicação ao utilizador.
        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    // Se Flag ativa, há violação de integridade relacional. Informa e devolve true.
        //    if (relationalViolationForFKtables)
        //    {
        //        Console.WriteLine(
        //            strBuilderFK.ToString(),    // Corpo da msg
        //            "Violação de Integridade Relacional"
        //        );
        //        return true;    // Há violação de integridade
        //    }
        //    return false;       // Não há violação de integridade
        //}
        #endregion
    }
}
