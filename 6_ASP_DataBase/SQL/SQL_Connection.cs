﻿
using System;
using System.Data.Common;

//----------------------------SQL
using System.Data.SqlClient;    // SQLServer e BDLocal
using MySql.Data.MySqlClient;   // MySQL
using Oracle.DataAccess.Client; // Oracle
using System.Data.SQLite;       // SQlite
using static _6_ASP_DataBase.SQL.DMBS;

namespace _6_ASP_DataBase.SQL
{
    static public class SQL_Connection
    {
        #region Dados locais

        static private readonly bool DEBUG_LOCAL = false;               // Ativa debug local

        // Regista o DBMS ativo, para execução de SQL DML
        static internal int DBMSactive { get; set; } = DBMS_NULL;

        // Connectors para ligar à base de dados
        static internal SqlConnection       conn_sqlserver = null;
        static internal MySqlConnection     conn_mysql = null;
        static internal OracleConnection    conn_oracle = null;
        static internal SQLiteConnection    conn_sqlite = null;

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        /// ConnectionStrings - Contêm o endereço e as credenciais de ligação, a associar ao connector
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        // LocalDB -Instância local SQL Server express do Visual Studio.
        static private readonly String sqlConnStringBdLocal = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\LocalDBs\LocalDB.mdf;Integrated Security=True;MultipleActiveResultSets=true";

        // SQLServer 2017 DBMS num dado IP.
        static private readonly String sqlConnStringSQLserver = @"Data Source=X240\SQLEXPRESS2017;Initial Catalog=Escola;Persist Security Info=True;User ID=root;Password=tgpsi";

        // MySQL
        static private readonly String sqlConnStringMySql = //@"server=localhost; database=turmas; user id=root; password=tgpsi; SslMode=none";
                                                            @"server=localhost; database=escola; user id=tgpsi; password=tgpsi; SslMode=none";


        // SQL lite: para manter a BD transportável em git, Não há um DBMS, mas sim uma file. Logo o caminho é absoluto:
        // Para o tornar adaptável a qualquer PC, com git ou outro qq meio, usar a seguinte concatenação:
        static private readonly String sqlConnStringSQLite = "data source=" + System.IO.Path.GetFullPath(@"..\..\LocalDBs\LocalDB_SQLite.db") + ";useutf16encoding=True;Version=3";

        // Oracle - por desenvolver.
        static private readonly String sqlConnStringOracle = @"";

        static internal string clipboardText = null;

        #endregion

        #region SQL Connections

        /// <summary>
        /// Overloaded
        /// Cria e devolve uma connection aberta para o DBMS ativo
        /// </summary>
        /// <param name="dbmsToActivate">DBMS a ativar. Usar as constantes de Settings para a chamada</param>
        /// <returns>Devolve a ligação "devolvida pelo verdadeiro método"</returns>
        static public DbConnection OpenConnection(int dbmsToActivate)
        {
            DBMSactive = dbmsToActivate;    // Ativa um DBMS
            return OpenConnection();        // Cria a ligação no método "verdadeiro" e devolve-a a quem pedir
        }

        /// <summary>
        /// Cria e devolve uma connection aberta para o DBMS ativo
        /// </summary>
        /// <returns>Ligação preparada e aberta</returns>
        static public DbConnection OpenConnection()
        {
            // Termina as Ligações antigas primeiro. False para não colocar DBMSactive a null
            CloseAllConnections(false);

            // Criação de um objeto de ligação comum a todos os connectors a devolver a quem pedir uma ligação
            DbConnection connection = null;

            try
            {
                // Efetua a ligação para o DBMS ativo
                switch (DBMSactive)
                {
                    case DBMS_SQLSERVER_BD_LOCAL:
                        // Prepara a Ligação SQLServer com a connectionString
                        conn_sqlserver = new SqlConnection(sqlConnStringBdLocal);

                        // tenta abrir a Ligação
                        conn_sqlserver.Open();

                        // Se abertura com sucesso, entrega a ligação ao objeto genérico. Caso contrário gera exceção
                        connection = conn_sqlserver;
                        DBMSactive = DBMS_SQLSERVER_BD_LOCAL;
                        break;

                    case DBMS_SQLSERVER:
                        //inicializa a Ligação
                        conn_sqlserver = new SqlConnection(sqlConnStringSQLserver);

                        //tenta abrir a Ligação
                        conn_sqlserver.Open();

                        // Se abertura com sucesso, entrega a ligação ao objeto genérico. Caso contrário gera exceção
                        connection = conn_sqlserver;
                        DBMSactive = DBMS_SQLSERVER;
                        break;

                    case DBMS_MYSQL:
                        //inicializa a Ligação
                        conn_mysql = new MySqlConnection(sqlConnStringMySql);

                        //tenta abrir a Ligação
                        conn_mysql.Open();

                        // Se abertura com sucesso, entrega a ligação ao objeto genérico. Caso contrário gera exceção
                        connection = conn_mysql;
                        DBMSactive = DBMS_MYSQL;
                        break;

                    case DBMS_ORACLE:
                        //inicializa a Ligação
                        conn_oracle = new OracleConnection(sqlConnStringOracle);

                        //tenta abrir a Ligação
                        conn_oracle.Open();

                        // Se abertura com sucesso, entrega a ligação ao objeto genérico. Caso contrário gera exceção
                        connection = conn_oracle;
                        DBMSactive = DBMS_ORACLE;
                        break;

                    case DBMS_SQLITE:
                        //inicializa a Ligação
                        conn_sqlite = new SQLiteConnection(sqlConnStringSQLite);

                        //tenta abrir a Ligação
                        conn_sqlite.Open();
                        
                        // Se abertura com sucesso, entrega a ligação ao objeto genérico. Caso contrário gera exceção
                        connection = conn_sqlite;
                        DBMSactive = DBMS_SQLITE;
                        break;

                    case DBMS_NULL:
                        Console.WriteLine(
                            "Atenção ao programador, DBMS is NULL",
                            "SQL_Connection - OpenConnectio() - case DBMS_NULL"
                        );
                        break;

                    default:
                        Console.WriteLine(
                            "Atenção ao programador, DBMS desconhecido:\n"+ DBMSactive,
                            "SQL_Connection - OpenConnectio() - Default"
                        );
                        break;
                }
            }
            catch (Exception ex)
            {
                // Mostra a mensegem de erro numa MsgBox e associa o botão ok ao Clipboard. 
                // Desta forma o utilizador poderá colar o erro na google e pesquisar soluções

                string msgtext = "Não foi possível ligar à base de dados, com o seguinte erro:\n\n\""
                    + ex.Message
                    + "\"\n\nContacte o Administrador do sistema informático"
                    +"\nO botão OK irá copiar a mensagem de erro. Google it!";

                Console.WriteLine(msgtext);

                // Como não se deve pode usar o ClipBord de forma direta a partir de uma thread, 
                // copiamos a msg de erro para um atributo desta classe static e acedemos-lhe a partir da 
                // thread principal após o encerramento da Thread 2
                clipboardText = ex.Message;

                return connection;
            }

            return connection;
        }

        /// <summary>
        /// Fecha todas as Ligações com os DBMS
        /// </summary>
        /// <param name="dbmsNull">True: passa o DBMSactive para null</param>
        static public void CloseAllConnections(bool dbmsActiveNull)
        {
            //fecha ligação sqlserver
            if (conn_sqlserver != null)
            {
                conn_sqlserver.Close();
                conn_sqlserver = null;
            }

            //fecha ligação mysql
            if (conn_mysql != null)
            {
                conn_mysql.Close();
                conn_mysql = null;
            }

            //fecha ligação Oracle
            if (conn_oracle != null)
            {
                conn_oracle.Close();
                conn_oracle = null;
            }

            //fecha ligação SQLite
            if (conn_sqlite != null)
            {
                conn_sqlite.Close();
                conn_sqlite = null;
            }

            if(dbmsActiveNull) DBMSactive = DBMS_NULL;     //DBMS passa a inativo
        }



        #endregion

        #region Utilitários

        /// <summary>
        /// Permite enviar parametros vazios se determinado valor acontecer
        /// Neste caso se o valor for null ou -1 é enviado um null para a bd
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        internal static object ValueOrDBNullIfZero(Int32? val)
        {
            if (val == null) return DBNull.Value;
            if (val == -1) return DBNull.Value;
            return val;
        }

        internal static object ValueOrDBNullIfZero(Object val)
        {
            if (val == null) return DBNull.Value;
            return val;
        }

        #endregion
    }
}
