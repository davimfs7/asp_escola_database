﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _6_ASP_DataBase
{
    public class User
    {

        public Int32 id { get; set; }
        public String name { get; set; }
        public Int32 codcivil { get; set; }
        public string morada { get; set; }
        public string dataNascimento { get; set; }

    }
}