using System;
/// <summary>
/// Classe de Processamento de dados relacionados com o Ciclo Letivo
/// Possui os atributos necess�rios � manipula��o dos dados da tabela
/// Possui dois construtores:
/// - m�nimo: com os atributos da tabela NOT NULL
/// - Completo: com todos os atributos da tabela
/// </summary>
internal class CicloLetivo {
	/// <summary>
	/// C�digo do Ciclo
	/// </summary>
	private int id;
	public int Id {
		get {
			return id;
		}
		set {
			id = value;
		}
	}
	/// <summary>
	/// descri��o do ano letivo
	/// </summary>
	private string descri;
	public string Descri {
		get {
			return descri;
		}
		set {
			descri = value;
		}
	}

	private Turma[] turma;

}
