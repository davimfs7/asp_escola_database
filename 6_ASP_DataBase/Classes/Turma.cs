using System;
/// <summary>
/// Classe de Processamento de dados relacionados com o Ciclo Letivo
/// Possui os atributos necess�rios � manipula��o dos dados da tabela
/// Possui dois construtores:
/// - m�nimo: com os atributos da tabela NOT NULL
/// - Completo: com todos os atributos da tabela
/// </summary>
internal class Turma {
	/// <summary>
	/// C�digo de Processo da Turma.
	/// </summary>
	private int id;
	public int Id {
		get {
			return id;
		}
		set {
			id = value;
		}
	}
	/// <summary>
	/// descri��o da turma relativa aos dados gerais da turma
	/// </summary>
	private string descri;
	public string Descri {
		get {
			return descri;
		}
		set {
			descri = value;
		}
	}
	/// <summary>
	/// ano letivo da turma
	/// </summary>
	private string anoLetivo;
	public string AnoLetivo {
		get {
			return anoLetivo;
		}
		set {
			anoLetivo = value;
		}
	}

	private CicloLetivo cicloLetivos;
	private Matricula[] matriculas;

}
