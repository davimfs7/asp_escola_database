using System;
/// <summary>
/// Super Classe Pessoa.
/// Serve a classe Aluno e outras que possam ser adicionadas.
/// </summary>
public class Pessoa {
	/// <summary>
	/// C�digo Civil da Pessoa.
	/// </summary>
	private int codCivil;
	public int CodCivil {
		get {
			return codCivil;
		}
		set {
			codCivil = value;
		}
	}
	/// <summary>
	/// Nome da Pessoa
	/// </summary>
	private string nome;
	public string Nome {
		get {
			return nome;
		}
		set {
			nome = value;
		}
	}
	/// <summary>
	/// Morada da Pessoa
	/// </summary>
	private string morada;
	public string Morada {
		get {
			return morada;
		}
		set {
			morada = value;
		}
	}
	/// <summary>
	/// Data de nascimento da Pessoa
	/// </summary>
	private DateTime dataNascimento;
	public DateTime DataNascimento {
		get {
			return dataNascimento;
		}
		set {
			dataNascimento = value;
		}
	}
	/// <summary>
	/// N� de Identifica��o da seguran�a Social da Pessoa
	/// </summary>
	private int niss;
	public int Niss {
		get {
			return niss;
		}
		set {
			niss = value;
		}
	}
	/// <summary>
	/// N� de Identifica��o Fiscal da Pessoa
	/// </summary>
	private int nif;
	public int Nif {
		get {
			return nif;
		}
		set {
			nif = value;
		}
	}
	/// <summary>
	/// Telefone Fixo da Pessoa
	/// </summary>
	private int telefoneFixo;
	public int TelefoneFixo {
		get {
			return telefoneFixo;
		}
		set {
			telefoneFixo = value;
		}
	}
	/// <summary>
	/// Telem�vel da Pessoa
	/// </summary>
	private int telemovel;
	public int Telemovel {
		get {
			return telemovel;
		}
		set {
			telemovel = value;
		}
	}
	/// <summary>
	/// Email da Pessoa
	/// </summary>
	private string email;
	public string Email {
		get {
			return email;
		}
		set {
			email = value;
		}
	}
	/// <summary>
	/// G�nero da Pessoa: Macho F�mea
	/// </summary>
	private bool genero;
	public bool Genero {
		get {
			return genero;
		}
		set {
			genero = value;
		}
	}
	/// <summary>
	/// Nacionalidade da Pessoa
	/// </summary>
	private string nacionalidade;
	public string Nacionalidade {
		get {
			return nacionalidade;
		}
		set {
			nacionalidade = value;
		}
	}
	/// <summary>
	/// Naturalidade da Pessoa
	/// Local de Nascimento
	/// </summary>
	private string naturalidade;
	public string Naturalidade {
		get {
			return naturalidade;
		}
		set {
			naturalidade = value;
		}
	}

    
    
}
