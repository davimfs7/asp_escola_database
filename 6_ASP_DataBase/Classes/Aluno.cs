using System;
/// <summary>
/// Classe de Processamento de dados relacionados com o Aluno
/// Possui os atributos necess�rios � manipula��o dos dados da tabela
/// Possui dois construtores:
/// - m�nimo: com os atributos da tabela NOT NULL
/// - Completo: com todos os atributos da tabela
/// </summary>
public class Aluno : Pessoa{

    /// <summary>
    /// C�digo de Processo do Aluno
    /// </summary>
    private int id;
    public int Id {
		get {
			return id;
		}
		set {
			id = value;
		}
	}

    /// <summary>
    /// Construtor vazio
    /// </summary>
    public Aluno()
    {

    }

    /// <summary>
    /// Construtor
    /// </summary>
    /// <param name="id"></param>
    /// <param name="nome"></param>
    /// <param name="dataNascimento"></param>
    /// <param name="genero"></param>
    public Aluno(int id, string nome, DateTime dataNascimento, bool genero)
    {
        this.id = id;
        this.Nome = nome;
        this.DataNascimento = dataNascimento;
        this.Genero = genero;
    }

}
