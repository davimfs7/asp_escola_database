using System;
/// <summary>
/// Matricula do Aluno ocorre por rela��o entre o Aluno e a Turma
/// PK=idAluno+idTurma
/// Possui o n� aluno na turma e data de matr�cula
/// Possui um construtor:Todos os atributos da tabela s�o NOT NULL
/// </summary>
internal class Matricula {
	/// <summary>
	/// n� aluno na turma
	/// </summary>
	private int nAluno;
	public int NAluno {
		get {
			return nAluno;
		}
		set {
			nAluno = value;
		}
	}
	/// <summary>
	/// Data de matr�cula do Aluno na Turma
	/// </summary>
	private DateTime dataMatricula;
	public DateTime DataMatricula {
		get {
			return dataMatricula;
		}
		set {
			dataMatricula = value;
		}
	}

	private Aluno aluno;
	private Turma turma;

}
